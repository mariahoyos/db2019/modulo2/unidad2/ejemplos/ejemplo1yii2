<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticias-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= GridView::widget([
        'dataProvider' => $data,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'], id que pone grid view

            'id',
            'titulo',
            'texto:ntext',//ntext es texto largo

            //['class' => 'yii\grid\ActionColumn'], botones de acción
        ],
    ]); ?>


</div>
