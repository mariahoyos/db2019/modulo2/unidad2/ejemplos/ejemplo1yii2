<?php

namespace app\controllers;

use Yii;
use app\models\Noticias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;




/**
 * NoticiasController implements the CRUD actions for Noticias model.
 */
class NoticiasController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Noticias models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }
    
        public function actionListado() {
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find(),
        ]);

        return $this->render('listar', [
                    'data' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Noticias model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Noticias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Noticias();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Noticias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Noticias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Noticias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Noticias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Noticias::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionConsulta1() {
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find()->select("titulo"),
        ]);

        return $this->render('index_1', [
                    'dataProvider' => $dataProvider,
             'titulo'=>'Titulo de las noticias',
            'descripcion' =>'SELECT titulo from noticias',
            'columnas'=>['titulo'],
        ]);
    }
    
    public function actionConsulta1a() {
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find()->select("texto"),
        ]);

        return $this->render('index_1', [
                    'dataProvider' => $dataProvider,
             'titulo'=>'Texto de las noticias',
            'descripcion' =>'SELECT texto from noticias',
            'columnas'=>['texto'],
        ]);
    }
    
        public function actionConsulta1b() {
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find()
                ->select("titulo")
                ->where('id<=2'),
            'pagination'=>[
                'pageSize'=>1,
            ]
        ]);

        return $this->render('index_1', [
                    'dataProvider' => $dataProvider,
             'titulo'=>'Título de las noticias con ID menor o igual que 2',
            'descripcion' =>'SELECT texto FROM noticias WHERE id<=2',
            'columnas'=>['titulo'],
        ]);
    }

    public function actionConsulta2() {
        $dataProvider = new ActiveDataProvider([
            'query' => Noticias::findBySql("select left(titulo,2)iniciales, titulo from noticias"),
            'pagination'=>[
                'pageSize'=>1,
            ]
        ]);

        return $this->render('index_1', [
            'dataProvider' => $dataProvider,
            'titulo'=>'Titulo de las noticias',
            'descripcion' =>'SELECT titulo from noticias',
            'columnas'=>['iniciales','titulo'],
        ]);
    }

    public function actionConsulta3() {

        $totalCount = Yii::$app->db
                ->createCommand('SELECT count(*) FROM noticias')
                ->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT titulo from noticias',
            'totalCount' => $totalCount,
            'pagination' => [
                'pageSize' => 2,
            ],
        ]);

        return $this->render('index_1', [
                    'dataProvider' => $dataProvider,
            'titulo'=>'Titulo de las noticias',
            'descripcion' =>'SELECT titulo from noticias',
            'columnas'=>['titulo'],
        ]);
    }
    
        public function actionConsulta3a(){
            $salida = Yii::$app->db->createCommand('SELECT titulo FROM noticias');
            //devolver el resultado a la vista    
            return $this->render('locura',[
                'datos'=>$salida->queryAll()
            ]);    
        }
        
        public function actionConsulta3b(){
            $numero = Yii::$app
                    ->db
                    ->createCommand('SELECT count(*) FROM noticias')
                    ->queryScalar();
            
            $dataProvider = new SqlDataProvider([
                'sql' => 'SELECT titulo from noticias',
                'totalCount' => $numero,
            ]);
            
            //devolver el resultado a la vista    
            return $this->render('index_1',[
                'dataProvider'=>$dataProvider,
                'titulo'=>'Titulo de las noticias',
                'descripcion' =>'SELECT titulo from noticias',
                'columnas'=>['titulo'],
            ]);    
        }
        
        public function actionConsulta4(){
            $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find()
                ->select("titulo, texto")
                ->distinct()
                ->where('id between 1 and 3'),
            ]);
            
            $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find()
                ->select("titulo, texto")
                ->distinct()
                ->where(["and",["<=","id",3],[">=","id",1]]),
            ]);

            return $this->render('index_1', [
                        'dataProvider' => $dataProvider,
                 'titulo'=>'Listado del título y texto de las noticias con id entre 1 y 3',
                'descripcion' =>'SELECT DISTINCT titulo, texto FROM noticias WHERE id BETWEEN 1 AND 3',
                'columnas'=>['titulo','texto'],
            ]); 
        }
        
        public function actionConsulta4a(){
            $numero = Yii::$app
                    ->db
                    ->createCommand('SELECT count(*) FROM noticias where id between 1 and 3')
                    ->queryScalar();
            
            $dataProvider = new SqlDataProvider([
                'sql' => 'SELECT DISTINCT titulo, texto FROM noticias WHERE id BETWEEN 1 AND 3',
                'totalCount' => $numero,
            ]);
            
            //devolver el resultado a la vista    
            return $this->render('index_1',[
                'dataProvider'=>$dataProvider,
                'titulo'=>'Listado del título y texto de las noticias con id entre 1 y 3',
                'descripcion' =>'SELECT DISTINCT titulo, texto FROM noticias WHERE id BETWEEN 1 AND 3',
                'columnas'=>['titulo','texto'],
            ]);    
        }
        //texto largo y corto de las noticias
        public function actionConsulta5(){
            $numero = Yii::$app
                    ->db
                    ->createCommand('SELECT count(*) FROM noticias')
                    ->queryScalar();
            
            $dataProvider = new SqlDataProvider([
                'sql' => 'SELECT concat(LEFT(texto,5),"...") corto, texto FROM noticias',
                'totalCount' => $numero,
            ]);
            
            //devolver el resultado a la vista    
            return $this->render('index_1',[
                'dataProvider'=>$dataProvider,
                'titulo'=>'Texto corto y completo de las noticias',
                'descripcion' =>'SELECT CONCAT(LEFT(texto,5),"...") corto, texto FROM noticias',
                'columnas'=>['corto','texto'],
            ]);    
        }
        public function actionConsulta5a(){
            $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find()
                ->select(['concat(left(texto,5),"...") corto','texto'])
            ]);
            
            

            return $this->render('index_1', [
                        'dataProvider' => $dataProvider,
                 'titulo'=>'Texto corto y completo de las noticias',
                'descripcion' =>'SELECT CONCAT(LEFT(texto,5),"...") corto, texto FROM noticias',
                'columnas'=>['corto','texto'],
            ]); 
        }
        public function actionConsulta6(){
            $dataProvider = new ActiveDataProvider([
            'query' => Noticias::find(),
            'pagination' => [
                'pageSize' => 1,
            ],   
            ]);
            
            return $this->render("listView", [
               "dataProvider"=>$dataProvider, 
            ]);
        }
        
    
}
    